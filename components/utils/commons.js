import womenJSON from './women.json';
export const women = womenJSON;

export const prodPath = function(){
  return process.env.NODE_ENV === 'production' ? '/test' : '';
}