import Link from 'next/link'

const linkStyle = {
  marginRight: 15
}

export default function Header({shouldDisplay}) {

  if (shouldDisplay) {
    return (
      <div>
        <Link href="/">
          <a style={linkStyle}>Home</a>
        </Link>
        <Link href="/about">
          <a style={linkStyle}>About</a>
        </Link>
      </div>
    ); 
  }
  return (null);
}
