import Layout from '../components/MyLayout';
import Link from 'next/link';
import {prodPath} from '../components/utils/commons';

console.log(prodPath());

const Index = props => (
  <Layout>
    <h1>The Point of View Paradox</h1>

    <p>
      This experiment demonstrates how our point of view powerfully conditions us and affects the way we interact with the world.
    </p>

    <ul>
        <li>
          <Link href={`${prodPath()}/women/[id]`} as={`${prodPath()}/women/old-woman`}>
            <a>Start Test</a>
          </Link> 
        </li>
        <li>
          <Link href="/women/[id]" as={`${prodPath()}/women/young-woman`}>
            <a>Start Test</a>
          </Link>
        </li> 
    </ul> 
  </Layout>
)

// Index.getInitialProps = async function() {
//   const res = await fetch('https://api.tvmaze.com/search/shows?q=batman')
//   const data = await res.json()

//   console.log(`Show data fetched. Count: ${data.length}`)

//   return {
//     shows: data.map(entry => entry.show)
//   }
// }

export default Index
