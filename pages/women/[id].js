import Layout from '../../components/MyLayout';
import {women} from '../../components/utils/commons';
import Link from 'next/link'
// import fetch from 'isomorphic-unfetch';

const Women = props => (
  <Layout> 
     
    <img src={props.woman} />
    <Link href="/women/woman" >
        <a>Next</a>
    </Link> 
  </Layout>
);

Women.getInitialProps = async function(context) {
  const { id } = context.query;

  return {woman:women[id]};
};

export default Women;
