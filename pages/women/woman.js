import Layout from '../../components/MyLayout';
import {women} from '../../components/utils/commons';
// import fetch from 'isomorphic-unfetch';

const Woman = ({woman}) => (
  <Layout> 
    <img src={woman} />
  </Layout>
);

Woman.getInitialProps = async function(context) { 
  return {woman:women['woman']};
};
 

export default Woman;
