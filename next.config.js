

const women = require('./components/utils/women.json');


 console.log(  process.env.NODE_ENV  );

module.exports = {  
  assetPrefix: process.env.NODE_ENV === 'production' ? '/test' : '',
  exportPathMap: async function() {
    const paths = {
      '/': { page: '/' },
      '/about': { page: '/about' },
      '/woman': { page: '/women/woman'}

    };

    // women.forEach(woman => {
    //   paths[`/women/${woman}`] = { page: '/women/[id]', query: { id: woman } };
    // }); 

    Object.keys(women).forEach(function(key) { 
      paths[`/women/${key}`] = { page: '/women/[id]', query: { id: women[key] } };
    })

    return paths;
  },
};